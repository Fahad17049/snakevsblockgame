package GuiEngine;

import javafx.scene.Scene;

public abstract class Scenes {
	protected int Xlimit = 500;
	protected  int Ylimit = 1000;
	protected  SnakeVsBlockGame myGame;
	protected  Scene myScene;
	public Scenes(SnakeVsBlockGame game) {
		this.myGame = game;
	}
	public abstract Scene giveScene();
}
