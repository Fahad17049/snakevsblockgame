package GuiEngine;

import java.io.FileInputStream;
import java.util.Random;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class Panel {
	public static final double Xlimit=500;
	public static final double Ylimit=1000;
	private Group root;
	private static int  flag=0;
	public Panel() {
		root=new Group();
		//root.setTranslateX(250);
		//root.setTranslateY(-50);
	}
	public Group createPanel() {
		//synchronized (this) {
			if(flag==1) {
				flag=0;
				return pane2();
			}
			Random a=new Random();
			int x=a.nextInt(100);
			if (x<=40) {
				flag=1;
				return pane1();
			}
			else if(x<=70)
			{
				return pane2();
			}
			else {
				return root;
			}
	//	}
		
		
	}
	public Group pane1() {
		
		StackPane sp1=new StackPane();
		sp1.setTranslateX(Xlimit/2);
		sp1.setTranslateY(-Ylimit/20);
		sp1.setPrefHeight(Ylimit/10);
		sp1.setPrefWidth(Xlimit);
		for(int i=0;i<5;i++) {
			Random ra=new Random();
			boolean x=ra.nextBoolean();
			if(x) {
				Rectangle s1=setup((i)*(Xlimit/5), -Ylimit/10, Ylimit/10, Xlimit/5,sp1);
				s1.setArcWidth(30.0); 
			    s1.setArcHeight(20.0);
			}
		}
		root.getChildren().add(sp1);
		return root;
	}
	public Group pane2() {
		StackPane sp1=new StackPane();
		sp1.setTranslateX(Xlimit/2);
		sp1.setTranslateY(-Ylimit/20);
		sp1.setPrefHeight(Ylimit/10);
		sp1.setPrefWidth(Xlimit);
		for(int i=0;i<5;i++) {
			Random ra=new Random();
			int x=ra.nextInt(100)+1;
			if(1<=x && x<=25) {
				Rectangle r=new Rectangle();
				r.setWidth(Xlimit/100);
				r.setHeight(Ylimit/10);
				r.setTranslateX((2*i)*(50));
				r.setTranslateY(-Ylimit/10);
				r.setFill(Color.WHITE);
				sp1.getChildren().add(r);
			}
			else if(26<=x && x<=50){
				int y=ra.nextInt(100)+1;	
				if(y<=68) {
					Circle c=new Circle();
					c.setTranslateX((2*i)*50);
					c.setTranslateY(-100);
					c.setRadius(10);
					c.setFill(Color.rgb(ra.nextInt(255), ra.nextInt(255), ra.nextInt(255)));
					Text t=new Text(Integer.toString(ra.nextInt(30)));
					t.setTranslateX(2*i*50);
					t.setTranslateY(-100);
					sp1.getChildren().addAll(c,t);
				}
				else if(y<=76) {
		        Image image = new Image(getClass().getResourceAsStream("Magnet.png"));
		        ImageView imageView = new ImageView(image);
		        imageView.setFitHeight(50);
		        imageView.setFitWidth(50);
		        imageView.setTranslateX(2*i*50);
		        imageView.setTranslateY(-100);
		        sp1.getChildren().addAll(imageView);
				}
				else if(y<=84) {
				Image image = new Image(getClass().getResourceAsStream("coin.png"));
		        ImageView imageView = new ImageView(image);
		        imageView.setFitHeight(30);
		        imageView.setFitWidth(30);
		        imageView.setTranslateX(2*i*50);
		        imageView.setTranslateY(-100);
		        sp1.getChildren().addAll(imageView);
				}
				else if(y<=92) {					
		        Image image = new Image(getClass().getResourceAsStream("shield.png"));
		        ImageView imageView = new ImageView(image);
		        imageView.setFitHeight(50);
		        imageView.setFitWidth(50);
		        imageView.setTranslateX(2*i*50);
		        imageView.setTranslateY(-100);
		        sp1.getChildren().addAll(imageView);
				}
				else {
					Image image = new Image(getClass().getResourceAsStream("DestroyBlock.png"));
		        ImageView imageView = new ImageView(image);
		        imageView.setFitHeight(50);
		        imageView.setFitWidth(50);
		        imageView.setTranslateX(2*i*50);
		        imageView.setTranslateY(-100);
		        sp1.getChildren().addAll(imageView);
				}
			}
		}
		root.getChildren().add(sp1);
		return root;
	}
	public Group getRoot() {
		return root;
	}
	public Rectangle setup(double x,double y,double h,double w,StackPane ss) {
		Random ra=new Random();
		Rectangle r1=new Rectangle();
		r1.setWidth(w);
		r1.setHeight(h);
		r1.setTranslateX(x);
		r1.setTranslateY(y);
		r1.setFill(Color.rgb(ra.nextInt(255), ra.nextInt(255), ra.nextInt(255)));
		r1.setStrokeWidth(2);
		Text tt=new Text(Integer.toString(ra.nextInt(100)));
		tt.setTranslateX(x);
		tt.setTranslateY(y);
		ss.getChildren().addAll(r1,tt);
		return r1;
	}
}
