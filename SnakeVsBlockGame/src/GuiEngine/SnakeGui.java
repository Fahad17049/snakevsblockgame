package GuiEngine;

import java.util.ArrayList;
import java.util.Observable;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class SnakeGui{
	private double CentreX;
	private double CentreY;
	private double Radius;
	private Circle LeftEye;
	private Circle RightEye;
	private ArrayList<Circle> Tail;
	private Circle Face;
	private int noOfCircle=0;
	private Group snakepane;
	public SnakeGui(double x,double y,double r,Group snakepane) {
		Face = new Circle(x, y, r,Color.CADETBLUE);
		LeftEye=new Circle(x-r/2,y-r/4,r/4,Color.BLACK);
		RightEye=new Circle(x+r/2,y-r/4,r/4,Color.BLACK);
		CentreX=x;
		CentreY=y;
		Radius=r;
		this.snakepane=snakepane;
		this.snakepane.getChildren().add(Face);
		this.snakepane.getChildren().add(LeftEye);
		this.snakepane.getChildren().add(RightEye);
		Tail=new ArrayList<>();
		AddChild();
		AddChild();
		AddChild();
		AddChild();
		AddChild();
	}
	public void AddChild() {
		noOfCircle+=1;
		Circle c=new Circle(CentreX,CentreY+(noOfCircle+1)*Radius,Radius/2,Color.BLUE);
		Tail.add(c);
		this.snakepane.getChildren().add(c);
	}
	public void MoveLeft() {
		CentreX-=15;
		for ( Node iterator : this.snakepane.getChildren()) {
			Circle current = (Circle) iterator ;
			current.setCenterX(current.getCenterX()-15);
		}
	}
 	public void MoveRight() {
		CentreX+=15;
		for ( Node iterator : this.snakepane.getChildren()) {
			Circle current = (Circle) iterator ;
			current.setCenterX(current.getCenterX()+15);
		}
	}
	public double getCentreX() {
		return CentreX;
	}
	public double getCentreY() {
		return CentreY;
	}
	public double getRadius() {
		return Radius;
	}
}
