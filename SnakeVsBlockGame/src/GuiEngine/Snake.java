package GuiEngine;

public class Snake {
	private final Player myplayer;
	private int NoOfBalls;
	private int length;
	
	public Snake(Player player) {
		this.myplayer = player;
		
	}
	
	public int getNoOfBalls() {
		return NoOfBalls;
	}
	public void setNoOfBalls(int noOfBalls) {
		NoOfBalls = noOfBalls;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public Player getMyplayer() {
		return myplayer;
	}

}
