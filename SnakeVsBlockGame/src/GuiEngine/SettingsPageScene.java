package GuiEngine;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class SettingsPageScene extends Scenes{
	public SettingsPageScene(SnakeVsBlockGame game) {
		super(game);
	}

	@Override
	public Scene giveScene() {
		Label Settings=new Label("SETTINGS");
		Settings.setId("Settings");
		Label Speed=new Label("SPEED:");
		Label Music=new Label("MUSIC");
		Label Sound=new Label("SOUND");
		Speed.setId("Speed");
		Music.setId("Music");
		Sound.setId("Sound");
		RadioButton SpeedSlow=new RadioButton("Slow");
		SpeedSlow.setId("Slow");
		SpeedSlow.setTranslateX(0);
		SpeedSlow.setTranslateY(-100);
		RadioButton SpeedModerate=new RadioButton("Moderate");
		SpeedModerate.setId("Moderate");
		RadioButton SpeedFast=new RadioButton("Fast");
		SpeedFast.setId("Fast");
		ToggleGroup SpeedGroup = new ToggleGroup();  
		SpeedSlow.setToggleGroup(SpeedGroup);
		SpeedModerate.setToggleGroup(SpeedGroup);
		SpeedFast.setToggleGroup(SpeedGroup);
		RadioButton MusicOn=new RadioButton("On");
		RadioButton MusicOff=new RadioButton("Off");
		RadioButton SoundOn=new RadioButton("On");
		RadioButton SoundOff=new RadioButton("Off");
		ToggleGroup MusicGroup = new ToggleGroup();  
		MusicOff.setToggleGroup(MusicGroup);
		MusicOn.setToggleGroup(MusicGroup);
		ToggleGroup SoundGroup = new ToggleGroup();  
		SoundOn.setToggleGroup(SoundGroup);
		SoundOff.setToggleGroup(SoundGroup);
		SpeedFast.setToggleGroup(SoundGroup);
		MusicOn.setId("MusicOn");
		MusicOff.setId("MusicOff");
		SoundOn.setId("SoundOn");
		SoundOff.setId("SoundOff");
		
		
		Button b=new Button("GoToMainPage");
		b.setId("Button");
		b.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				myGame.setPrimaryStage(myGame.GetMainPageScene());
			}
		});
		
		StackPane sp=new StackPane();
		sp.getChildren().addAll(Settings,Speed,Music,Sound,SpeedSlow,SpeedFast,SpeedModerate,MusicOff,MusicOn,SoundOff,SoundOn,b);
		sp.setId("pane");
		myScene = new Scene(sp,500,1000);
		myScene.setFill(Color.BLACK);
		sp.getStylesheets().add(getClass().getResource("SettingsPage.css").toExternalForm());
		return myScene;
	}
	
	
}
