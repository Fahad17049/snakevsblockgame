package GuiEngine;

import java.util.ArrayList;

import javafx.animation.AnimationTimer;

public class GameController {
	private ArrayList<Tokens> currentTokens;
	private Player currentPlayer;
	private ArrayList<Hittable> currentHittables;
	private int currentScore;
	
	public GameController() {
		AnimationTimer timer = new AnimationTimer() {
			
			@Override
			public void handle(long now) {
				update();
			}

		};
	timer.start();
	}
	public void update() {
		for (Tokens tokens: currentTokens) {
			
		}
		for (Hittable hittable: currentHittables ) {
			
		}
		
		
	}
	
	
	
	
	public ArrayList<Tokens> getCurrentTokens() {
		return currentTokens;
	}
	public void setCurrentTokens(ArrayList<Tokens> currentTokens) {
		this.currentTokens = currentTokens;
	}
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
	public ArrayList<Hittable> getCurrentHittables() {
		return currentHittables;
	}
	public void setCurrentHittables(ArrayList<Hittable> currentHittables) {
		this.currentHittables = currentHittables;
	}
	public int getCurrentScore() {
		return currentScore;
	}
	public void setCurrentScore(int currentScore) {
		this.currentScore = currentScore;
	}
	
}
