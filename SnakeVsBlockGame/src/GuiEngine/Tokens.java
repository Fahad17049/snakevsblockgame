package GuiEngine;

import javafx.scene.shape.Shape;

public abstract class Tokens extends Shape{
	private int posX;
	private int posY;
	
	public Tokens() {
	}
	
	public abstract void hit(Snake s);

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}
	
}
