package GuiEngine;

public interface Hittable {
	public void hit(Snake s);
}
