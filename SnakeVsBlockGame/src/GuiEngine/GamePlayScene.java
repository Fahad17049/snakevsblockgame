package GuiEngine;

import java.util.ArrayList;
import java.util.Random;

//import application.SnakeVsBlockGame.KeyPressedHandler;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class GamePlayScene extends Scenes{
	public GamePlayScene(SnakeVsBlockGame game) {
		super(game);
	}

	@Override
	public Scene giveScene() {
		BorderPane GameplayStackPane = new BorderPane();
		GameplayStackPane.setId("BorderPane");
		GameplayStackPane.setTranslateX(-200);
		GameplayStackPane.setTranslateY(0);
		myScene= new Scene(GameplayStackPane,Xlimit,Ylimit); // root of this scene is a stack pane.
		myScene.setFill(Color.BLACK);
//		Text scoreText = new Text("SCORE: 00");
//		scoreText.setTranslateX(150);
//		scoreText.setTranslateY(-370);
//		scoreText.setId("text");
	//	GameplayStackPane.getChildren().add(scoreText);
				Group snake = new Group();
		SnakeGui snakeGui = new SnakeGui(250, 500, 15, snake);
		GameplayStackPane.getChildren().add(snake);
		
		Button b=new Button("GoToMainPage");
		b.setId("Button");
		b.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				myGame.setPrimaryStage(myGame.GetMainPageScene());
			}
		});
		Menu menu = new Menu("");
		menu.setId("menuPlay");
		MenuItem Gotomainpage = new MenuItem("Exit");
		MenuItem Restart = new MenuItem("Restart");
		
		menu.getItems().add(Gotomainpage);
		menu.getItems().add(Restart);
		
		Menu score=new Menu("Score: 00");
		score.setId("text");
		Gotomainpage.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				myGame.setPrimaryStage(myGame.GetMainPageScene());
			}
		});
		
		Restart.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				myGame.setPrimaryStage(myGame.GetGamePlayScene());
				
			}
		});
		
		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().addAll(menu,score);
		VBox vb=new VBox(menuBar);
		vb.setPrefHeight(10);
		vb.prefWidth(50);
		vb.setTranslateX(200);
		vb.setTranslateY(0);
		GameplayStackPane.setCenter(vb);
		
		
		
		GameplayStackPane.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		myScene.setOnKeyPressed(new KeyPressedHandler(snake));
		setBackground(myScene);
		return myScene;
	}
	public void setBackground(Scene ss) {
		final BorderPane root=(BorderPane)ss.getRoot();
		Timeline t1=new Timeline();
		t1.setCycleCount(Animation.INDEFINITE);
		Timeline t2=new Timeline();
		t2.setCycleCount(Animation.INDEFINITE);
		ArrayList<HBox> HBoxArray=new ArrayList<>();
		KeyFrame k1=new KeyFrame(Duration.seconds(1), 
				new EventHandler<ActionEvent>(){
					@Override
					public void handle(ActionEvent arg0) {
						Random ra=new Random();
						HBox h1=new HBox();
						h1.setPrefHeight(Ylimit/10);
						h1.setPrefWidth(Xlimit);
						h1.setTranslateX(0);
						h1.setTranslateY((Ylimit/25));
						Panel p=new Panel();
						h1.getChildren().add(p.createPanel());
						root.getChildren().add(h1);
						HBoxArray.add(h1);
					}
			
				}
				);
		KeyFrame k2=new KeyFrame(Duration.seconds(0.02),
				new EventHandler<ActionEvent>(){

					@Override
					public void handle(ActionEvent arg0) {
						for(HBox h: HBoxArray) {
							h.setTranslateY(h.getTranslateY()+1);
							Group r=(Group)h.getChildren().get(0);
							r.setTranslateY(r.getTranslateY()+1);
						}
					}
				}
				);
		t1.getKeyFrames().add(k1);
		t1.play();
		t2.getKeyFrames().add(k2);
		t2.play();
	}
	private class KeyPressedHandler implements EventHandler<KeyEvent>{
		private Group mysnake;
		public KeyPressedHandler(Group org) {
			mysnake=org;
		}
		@Override
		public void handle(KeyEvent e) {
			switch (e.getCode()) {
			case LEFT: 
				if(mysnake.getTranslateX()>=0+15) {
				TranslateTransition translateCircle=new TranslateTransition();
				translateCircle.setNode(mysnake);
				translateCircle.setByX(-50);
				translateCircle.setDuration(Duration.millis(500));
				translateCircle.play();}
				break;
			case RIGHT: 
				if(mysnake.getTranslateX()<=500-15) {
				TranslateTransition translateCircleR=new TranslateTransition();
				translateCircleR.setNode(mysnake);
				translateCircleR.setByX(50);
				translateCircleR.setDuration(Duration.millis(500));
				translateCircleR.play();
				}
				break;
			default:
				break;
			};
		}
	}
}
