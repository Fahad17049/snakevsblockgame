package GuiEngine;
	
//import java.awt.Panel;
import java.util.ArrayList;
import java.util.Random;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;


public class SnakeVsBlockGame extends Application {
	public final int Xlimit = 500;
	public final int Ylimit = 1000;
	private  Stage PrimaryStage;
	public void setPrimaryStage(Scene scene) {
		this.PrimaryStage.hide();
		this.PrimaryStage = new Stage();
		PrimaryStage.setScene(scene);
		PrimaryStage.setTitle("SnakeVsBlockGame");
		PrimaryStage.show();
	}
	@Override
	public void start(Stage primaryStage) {
		try {
			this.PrimaryStage = primaryStage;
			primaryStage.setScene(GetMainPageScene());
			primaryStage.setTitle("SnakeVsBlockGame");
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public Scene GetMainPageScene() {
		MainPageScene mainPageScene = new MainPageScene(this);
		return mainPageScene.giveScene();
	}
	public Scene GetGamePlayScene() {
		GamePlayScene gamePlayScene = new GamePlayScene(this);
		return gamePlayScene.giveScene();
	}
	public Scene GetSettingsPageScene() {
		SettingsPageScene settingsPageScene = new SettingsPageScene(this);
		return settingsPageScene.giveScene();
	}
	public Scene GetLeaderBoardPageScene() {
		LeaderBoardPageScene leaderBoardPageScene = new LeaderBoardPageScene(this); 
		return leaderBoardPageScene.giveScene();
	}
	public Scene GetGameOverScene() {
		GameOverScene gameOverScene = new GameOverScene(this);
		return gameOverScene.giveScene();
	}
	

	
}
