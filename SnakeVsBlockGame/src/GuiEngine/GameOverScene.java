package GuiEngine;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

public class GameOverScene extends Scenes{
	public GameOverScene(SnakeVsBlockGame game) {
		super(game);
	}

	@Override
	public Scene giveScene() {
		StackPane p=new StackPane();
		p.setId("mypane");
		p.getStylesheets().add(getClass().getResource("GameOver.css").toExternalForm());
		Label score =new Label("Score: 00");
		score.setTranslateX(0);
		score.setTranslateY(-350);
		score.setTextFill(Color.BLUE);
		score.setId("score");
		
		
		Button b=new Button("GoToMainPage");
		b.setId("Button");
		b.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				myGame.setPrimaryStage(myGame.GetMainPageScene());
			}
		});
		
		
		p.getChildren().addAll(score,b);
		myScene= new Scene(p,500,1000);
		myScene.setFill(Color.BLACK);
		return null;
	}
	
}
