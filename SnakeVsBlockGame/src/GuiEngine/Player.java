package GuiEngine;

public class Player {
	private  final String name;
	private int maxScore;
	private final String password;
	private int currentScore;
	private Snake mysnake;
	
	public Player(String name, String password){
		this.name = name;
		this.password = password;
		this.mysnake = new Snake(this);
	}
	
	public String getName() {
		return name;
	}

	public int getMaxScore() {
		return maxScore;
	}
	public void setMaxScore(int maxScore) {
		this.maxScore = maxScore;
	}
	public String getPassword() {
		return password;
	}

	public int getCurrentScore() {
		return currentScore;
	}
	public void setCurrentScore(int currentScore) {
		this.currentScore = currentScore;
	}
	public Snake getMysnake() {
		return mysnake;
	}

}
